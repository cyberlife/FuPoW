pragma solidity ^0.4.16;

import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/interfaces/ICommonToken.sol";
import "contracts/interfaces/ITokenPolicy.sol";

contract ClientManager is Ownable {

    address commonToken;

    mapping(string => address) subContracts;

    function ClientManager() public {}
    
    function setCommonToken(address _common) public onlyOwner {

        commonToken = _common;

    }

    function addSubContract(address place, string name) public onlyOwner {

        subContracts[name] = place;

    }

    //COMMON TOKEN

    function raiseMintLimit(address who, string reason) public onlyOwner {

        ITokenPolicy policy = ITokenPolicy(getTokenPolicy());

        ICommonToken common = ICommonToken(commonToken);

        common.raiseMintingLimit(who, policy.getRewardCase(reason), reason);

    }

    function lowerMintLimit(address who, string reason) public onlyOwner {

        ITokenPolicy policy = ITokenPolicy(getTokenPolicy());

        ICommonToken common = ICommonToken(commonToken);

        common.raiseMintingLimit(who, policy.getSlashingCase(reason), reason);

    }

    function mint(address who, uint256 amount) public onlyOwner {

        ICommonToken common = ICommonToken(commonToken);

        common.mintFromClient(who, amount);

    }

    function mintAndTransfer(address who, address target, uint256 amount) public onlyOwner {

        ICommonToken common = ICommonToken(commonToken);

        common.mintAndTransferFromClient(who, target, amount);

    }

    //GET SPECIFIC CONTRACTS

    function getUserDatabase() public view returns (address) {

        return subContracts["userDatabase"];

    }

    function getTokenPolicy() public view returns (address) {

        return subContracts["tokenPolicy"];

    }

    function getClientMining() public view returns (address) {

        return subContracts["clientMining"];

    }

}