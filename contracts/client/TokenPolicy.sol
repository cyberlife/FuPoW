pragma solidity ^0.4.16;

import "contracts/interfaces/ITokenPolicy.sol";

contract TokenPolicy is ITokenPolicy {

    mapping(string => uint256) slashes;
    mapping(string => uint256) rewards;

    function TokenPolicy() {}

    function changeSlashingCase(string slashCase, uint256 tokenAmount) public onlyOwner {

        slashes[slashCase] = tokenAmount;

        ChangedSlashingCase(slashCase, tokenAmount);

    }

    function changeRewardCase(string rewardCase, uint256 tokenAmount) public onlyOwner {

        rewards[rewardCase] = tokenAmount;

        ChangedRewardCase(rewardCase, tokenAmount);

    }

    function getSlashingCase(string which) public view returns (uint256) {

        return slashes[which];

    }

    function getRewardCase(string which) public view returns (uint256) {

        return rewards[which];

    }

}