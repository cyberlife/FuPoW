pragma solidity ^0.4.16;

import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/zeppelin/ERC20/StandardToken.sol";

contract CyberTreasury is Ownable {

    event TransferredFundsToAdvancedContract(address advanced, uint256 howMuch);
    event SetMineable(address mineable);

    address mineable;

    string public base = "03/08/2018 On the brink of liberating software from creator";

    function CyberTreasury() {}

    function setMineableToken(address _mineable) public onlyOwner {

        mineable = _mineable;

        SetMineable(mineable);

    }

    function transferToAdvancedContract(address whichContract, uint256 howMuch) public onlyOwner {

        StandardToken token = StandardToken(mineable);

        token.transfer(whichContract, howMuch);

        TransferredFundsToAdvancedContract(whichContract, howMuch);

    }

}