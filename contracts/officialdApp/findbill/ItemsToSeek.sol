pragma solidity ^0.4.16;

import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/interfaces/IItemsToSeek.sol";

contract ItemsToSeek is Ownable, IItemsToSeek {

    string[] itemHashes;

    function ItemsToSeek() {

    }

    function addItem(string item) public onlyOwner {

        itemHashes.push(item);

        PushedItem(item, msg.sender);

    }

    function getItem(uint256 position) public constant returns (string) {

        return itemHashes[position];

    }

    function getItemsNumber() public constant returns (uint256) {

        return itemHashes.length;

    }

}