pragma solidity ^0.4.16;

import "contracts/officialdApp/findbill/ItemsToSeek.sol";
import "contracts/zeppelin/ownership/Ownable.sol";

contract Sessions is Ownable {

    event NewSession(uint256 start, uint256 end, uint256 subsessions, string name, 
             uint256 latitude, uint256 longitude, uint256 radius, uint256 prize);

    event SomeoneWon(string _name, uint256 prize);

    event NobodyWonAndWithdrew(uint256 startTime, string _name);

    struct Session {

        bool alreadyWon;

        string hashOfWinner;

        uint256 startTime;
        uint256 endTime;
        uint256 subsessions;

        uint256 latitude;
        uint256 longitude;
        uint256 radiusInMeters;

        uint256 prize;

        bool alreadyTakenPrize;

        address itemsLocation;

    }

    mapping(string => Session[]) activatedSessions;

    function Sessions() {}

    function newSession(uint256 start, uint256 duration, uint256 subsessions, string name, 
            uint256 latitude, uint256 longitude, uint256 radius) public payable onlyOwner {

        require(start > now);
        require(duration >= 60);
        require(subsessions >= 1);
        require(bytes(name).length > 0);
        require(msg.value > 0);
    
        ItemsToSeek itemsLocation = new ItemsToSeek();

        Session memory nextSession = Session(false, "Nobody", start, start + duration * 1 seconds, 
                                            subsessions, latitude, longitude, radius, msg.value, 
                                            false, address(itemsLocation));

        activatedSessions[name].push(nextSession);

        NewSession(start, start + duration * 1 seconds, subsessions, name, 
             latitude, longitude, radius, msg.value);

    }

    function addItem(string _name, uint256 position, address itemsAddr, string itemHash) public onlyOwner {

        require(bytes(_name).length > 0);
        require(now < activatedSessions[_name][position].startTime);

        ItemsToSeek items = ItemsToSeek(itemsAddr);

        items.addItem(itemHash);

    }

    function foundTheResponse(string _name, uint256 position, string who) public onlyOwner {

        require(bytes(who).length > 0);
        require(bytes(_name).length > 0);
        require(activatedSessions[_name][position].endTime > now);
        require(now >= activatedSessions[_name][position].startTime);

        activatedSessions[_name][position].alreadyWon = true;

        activatedSessions[_name][position].hashOfWinner = who;

    }

    function claimPrize(string _name, uint256 position, address _who) public onlyOwner {

        require(bytes(_name).length > 0);

        _who.transfer(activatedSessions[_name][position].prize);

        activatedSessions[_name][position].alreadyTakenPrize = true;

        SomeoneWon(_name, activatedSessions[_name][position].prize);

    }

    function nobodyWonSoWithdraw(string _name, uint256 position) public onlyOwner {

        require(bytes(_name).length > 0);
        require(activatedSessions[_name][position].alreadyWon == false);
        require(activatedSessions[_name][position].endTime + 24 hours < now);
     
        msg.sender.transfer(activatedSessions[_name][position].prize);

        NobodyWonAndWithdrew(activatedSessions[_name][position].startTime, _name);

    }

    function getStartTime(string name, uint256 position) public constant returns (uint256) {

        return activatedSessions[name][position].startTime;

    }

    function getEndTime(string name, uint256 position) public constant returns (uint256) {

        return activatedSessions[name][position].endTime;

    }

    function getPrize(string name, uint256 position) public constant returns (uint256) {

        return activatedSessions[name][position].prize;

    }

    function getWinner(string name, uint256 position) public constant returns (string) {

        return activatedSessions[name][position].hashOfWinner;

    }

    function prizeAlreadyTaken(string name, uint256 position) public constant returns (bool) {

        return activatedSessions[name][position].alreadyTakenPrize;

    }

    function getItemsLocation(string name, uint256 position) public constant returns (address) {

        return activatedSessions[name][position].itemsLocation;

    }

    function getDuration(string name, uint256 position) public constant returns (uint256) {

        return getEndTime(name, position) - getStartTime(name, position);

    }

    function getTime() public constant returns (uint256) {

        return now;

    }

}