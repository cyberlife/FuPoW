pragma solidity ^0.4.16;

import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/interfaces/IPlayerDatabase.sol";
import "contracts/zeppelin/math/SafeMath.sol";
import "contracts/interfaces/IFuPoW.sol";
import "contracts/interfaces/IHashRate.sol";

contract ClientMining is Ownable {

    using SafeMath for uint256;

    event Worked(string proof, bool success, address who, uint256 howManyHashes, uint256 timestamp);
    event ModifiedRevenueShare(uint256 forDappOwner, uint256 forUsers);

    uint256 successes;

    //Dapp devs should set this to zero; this way users get more incentivised to use their dapps and the coins get in more hands, not to one platform
    uint256 percentageRevenueDappOwner;

    uint256 percentageRevenueUser;

    //Recommended time between revenue split modification (in weeks); not too long, not too short
    uint256 timeUntilNextPercentageModification = 50;

    uint256 nextChangeThreshold = 0;

    uint256 totalHashNumber;

    address userDatabase;
    address protocol;

    address hashContract;

    function ClientMining(address _userDatabase, address _protocol, 
        uint256 _timeUntilNextPercentageModification, address globalHash) {

        userDatabase = _userDatabase;

        protocol = _protocol;

        timeUntilNextPercentageModification = _timeUntilNextPercentageModification;

        hashContract = globalHash;

    }

    function modifyRevenueShares(uint256 _dappOwner) public onlyOwner {

        require(now >= nextChangeThreshold);
        require(_dappOwner <= 10);

        percentageRevenueDappOwner = _dappOwner;
        percentageRevenueUser = 100 - _dappOwner;

        nextChangeThreshold = now + timeUntilNextPercentageModification * 1 weeks;

        ModifiedRevenueShare(_dappOwner, 100 - _dappOwner);

    }

    function work(string proof, bool success, address who, uint256 howManyHashes, uint256 timestamp) public onlyOwner {

        require(percentageRevenueUser > 0);

        if (success == true) {

            require(bytes(proof).length > 0);

        }

        require(who != owner);

        require(howManyHashes > 0);

        require(timestamp <= now + 1 minutes && timestamp >= now - 1 minutes); 

        totalHashNumber = totalHashNumber.add(howManyHashes);

        IHashRate hashes = IHashRate(hashContract);

        hashes.addClientSideHash(howManyHashes);

        if (success == true) {

            successes = successes.add(1);

            IFuPoW fupow = IFuPoW(protocol);

            fupow.foundSolution(msg.sender, who, percentageRevenueDappOwner, timestamp);

        }

        Worked(proof, success, who, howManyHashes, timestamp);

    }

    function addHashContract(address _hash) public onlyOwner {

        hashContract = _hash;

    }

    function getHashRate() public view returns (uint256) {

        return totalHashNumber;

    }

    function getProtocol() public view returns (address) {

        return protocol;

    }

    function getNextChangeThreshold() public view returns (uint256) {

        return nextChangeThreshold;

    }

    function getUserRevenueShare() public view returns (uint256) {

        return percentageRevenueUser;

    }

    function getPlatformRevenueShare() public view returns (uint256) {

        return percentageRevenueDappOwner;

    }

}