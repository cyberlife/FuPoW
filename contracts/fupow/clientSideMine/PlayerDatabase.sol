pragma solidity ^0.4.16;

import "contracts/zeppelin/math/SafeMath.sol";
import "contracts/interfaces/IPlayerDatabase.sol";

contract PlayerDatabase is IPlayerDatabase {

    using SafeMath for uint256;

    mapping(string => string) logincreds;
    mapping(string => address) addressForEmail;
    mapping(string => address) phoneNumbers;

    mapping(address => bool) realPerson;

    address sys_admin;
    address fupow;

    modifier onlySysAdmin {

        require(msg.sender == sys_admin);
        _;

    }

    function PlayerDatabase() {

        sys_admin = msg.sender;

    }

    function setProtocol(address _fupow) public onlySysAdmin {

        fupow = _fupow;

    }

    function fullSignup(string emailhash, string passhash, address assignedPublicKey, string phoneNumber) public onlySysAdmin {

        logincreds[emailhash] = passhash;
        addressForEmail[emailhash] = assignedPublicKey;
        phoneNumbers[phoneNumber] = assignedPublicKey;

        FullSignUp(emailhash, passhash, assignedPublicKey, phoneNumber);

    }

    function emailAssignKey(string emailhash, address assignedPublicKey) public onlySysAdmin {

        addressForEmail[emailhash] = assignedPublicKey;

        AssignKeyToEmail(emailhash, assignedPublicKey);

    }

    function numberAssignKey(string numberHash, address assignedPublicKey) public onlySysAdmin {

        phoneNumbers[numberHash] = assignedPublicKey;

        AssignKeyToNumber(numberHash, assignedPublicKey);

    }

    function login(string emailhash) public constant returns (string) {

       if (bytes(logincreds[emailhash]).length > 0) {
           
           return logincreds[emailhash];
           
       }

       return "0x0";

    }

    function checkIfNumberIsAssigned(string numberHash) public constant returns (bool) {

        if (phoneNumbers[numberHash] != address(0)) {return true;}
        return false; 

    }

    function getAddressFromEmail(string emailhash) public constant returns (address) {

        return addressForEmail[emailhash];

    }

    function getAddressFromNumber(string numberHash) public constant returns (address) {

        return phoneNumbers[numberHash];

    }

}