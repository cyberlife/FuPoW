pragma solidity ^0.4.16;

import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/tokens/CommonToken.sol";
import "contracts/interfaces/IHashRate.sol";
import "contracts/interfaces/IFuPoW.sol";

//TODO: token curated registry to curate the platforms that send requests

/*

    Open Programming Interface for FuPoW

    Dapps can submit requests in the form of hashes of images or videos that their users submitted

    If a video or hash contains the current object from FuPoW, the dapp gets to mine tokens and the tokens will be split between the dapp and the data creator

*/

contract FuOPI is Ownable {

    event NextRequest(address mpc, int nextToBeAnalyzed);
    event SubmitRequest(uint256 position, uint256 payment, string requestHash, uint256 mediaType, uint256 howManyPieces, address user);
    event AcceptRequest(int which, string proof, uint256 timestamp);
    event RejectRequest(int which, string proof, uint256 reason, uint256 timestamp);

    enum MediaTypes {

        IMAGES,
        VIDEOS,
        POINT_CLOUD

    }

    // If a request is rejected, these are the reasons; dapps are not allowed to submit material that includes human faces
    // Faces are not allowed in order to preserve the privacy of people

    enum RejectReasons {

        OBJECT_NOT_FOUND,
        TOO_MANY_OBJECTS,
        HUMAN_FACE_PRESENT

    }

    mapping (address => bool) consumerPlatforms;
    mapping (address => bool) mpcGroups;
    mapping (address => int) analysis;

    //Anyone can flag a platform as toxic or as a platform that did not ask the users if they let the platform send their content
    mapping (address => string[]) flags;

    struct Request {

        uint256 waitThreshold;

        address platform;
        address user;

        uint256 platformShare;
        uint256 userShare;

        string requestHash;
        uint256 howManyPieces;

        uint256 payment;
        uint256 locked;

        bool severeRejectReason;
        bool processed;

    }

    Request[] requests;

    address protocol;
    address paymentToken;
    address tokenSink;
    address priceContract;
    address hashes;

    //Per image or video; locked up until an image or video is analyzed and deemed not with human faces
    uint256 lockupFee = 500;

    //If someone pays, their submission is available 30 minutes (in the beginning of the protocol, 6 Levels)
    //If it's not processed in this timeframe, the platform who submitted can withdraw everything

    uint256 waitTime = 30 minutes;

    modifier onlyMPC {

        require(mpcGroups[msg.sender] == true);
        _;

    }

    modifier onlyAllowedPlatform {

        require(consumerPlatforms[msg.sender] == true);
        _;

    }

    modifier onlyPriceContract {

        require(msg.sender == priceContract);
        _;

    }

    function FuOPI(address _protocol, address _paymentToken, address _priceContract, address _hashes) {

        protocol = _protocol;
        paymentToken = _paymentToken;
        priceContract = _priceContract;
        hashes = _hashes;

    }

    function setHashContract(address _hashes) public onlyOwner {

        hashes = _hashes;

    }

    function setPriceContract(address _contract) public onlyOwner {

        priceContract = _contract;

    }

    function setTokenSink(address sink) public onlyOwner {

        tokenSink = sink;

    }

    function toggleMPCGroup(address group) public onlyOwner {

        require(isContract(group) == true);

        mpcGroups[group] = !mpcGroups[group];

        if (mpcGroups[group] == true) {

            analysis[group] = -1;

        }

    }

    function setPaymentToken(address token) public onlyOwner {

        paymentToken = token;

    }

    function lowerLockupFee(uint256 lowerLockup) public onlyOwner {

        require(lowerLockup < lockupFee);

        lockupFee = lowerLockup;

    }

    function analyzeNextRequest(int request) public onlyMPC {

        require(analysis[msg.sender] == -1);

        require(request >= 0);

        require(requests[uint256(request)].howManyPieces > 0);

        analysis[msg.sender] = request;

        NextRequest(msg.sender, request);

    }

    function submitRequest(uint256 tokenNumber, string requestHash, uint256 mediaType, 
        uint256 howManyPieces, address user, uint256 platformShare, uint256 userShare) public onlyAllowedPlatform {

        require(tokenNumber > 0);
        require(bytes(requestHash).length > 0);
        require(mediaType <= 2);
        require(howManyPieces > 0);
        require(user != address(0));

        //Add hashes

        IHashRate hashes = IHashRate(hashes);

        hashes.addOPIHash(howManyPieces);

        //Pay for processing and the lockupFee

        CommonToken token = CommonToken(paymentToken);

        uint256 adjustedLockup = lockupFee * (10 ** token.decimals());
        uint256 adjustedTokenNumber = tokenNumber * (10 ** token.decimals());

        require(token.allowance(msg.sender, this) >= adjustedLockup + adjustedTokenNumber);

        token.transferFrom(msg.sender, this, adjustedLockup + adjustedTokenNumber);

        //Create the request

        Request memory newRequest = Request(now + waitTime, msg.sender, user, platformShare, userShare, 
                                            requestHash, howManyPieces, adjustedTokenNumber, adjustedLockup, 
                                            false, false);

        requests.push(newRequest);

        SubmitRequest(requests.length - 1, adjustedTokenNumber, requestHash, mediaType, howManyPieces, user);

    }

    function acceptRequest(int which, string proof, uint256 timestamp) public onlyMPC {

        require(analysis[msg.sender] == which);
        require(bytes(proof).length > 0);
        require(timestamp <= now + 1 minutes && timestamp >= now - 1 minutes);

        CommonToken common = CommonToken(paymentToken);

        common.transfer(msg.sender, requests[uint256(which)].payment);

        requests[uint256(which)].payment = 0;

        IFuPoW pow = IFuPoW(protocol);

        pow.foundSolution(requests[uint256(which)].platform, requests[uint256(which)].user, 
                          requests[uint256(which)].platformShare,
                          timestamp);

        requests[uint256(which)].processed = true;

        analysis[msg.sender] = -1;

        AcceptRequest(which, proof, timestamp);

    }

    function rejectRequest(int which, string proof, uint256 reason, uint256 timestamp) public onlyMPC {

        require(analysis[msg.sender] == which);
        require(bytes(proof).length > 0);
        require(timestamp <= now + 1 minutes && timestamp >= now - 1 minutes);

        CommonToken common = CommonToken(paymentToken);

        common.transfer(msg.sender, requests[uint256(which)].payment);

        requests[uint256(which)].payment = 0;

        if (reason == 2) {

            requests[uint256(which)].severeRejectReason = true;

            common.transfer(tokenSink, requests[uint256(which)].locked);

            requests[uint256(which)].locked = 0;

        }

        requests[uint256(which)].processed = true;

        analysis[msg.sender] = -1;

        RejectRequest(which, proof, reason, timestamp);

    }

    function withdrawPayment(uint256 which) public {

        require(requests[which].waitThreshold >= now);
        require(requests[which].platform == msg.sender);
        require(requests[which].locked + requests[which].payment > 0);
        
        uint256 toWithdraw = 0;

        if (requests[which].processed == true) {

            if (requests[which].locked > 0) {

                toWithdraw = toWithdraw + requests[which].locked;

            }

        } else {

            toWithdraw = toWithdraw + requests[which].locked + requests[which].payment;

        }

        if (toWithdraw > 0) {

             CommonToken common = CommonToken(paymentToken);

             common.transfer(msg.sender, toWithdraw);

        }

        requests[which].locked = 0;
        requests[which].payment = 0;

    }

    function isContract(address addr) private returns (bool) {

        uint size;
        assembly { size := extcodesize(addr) }
        return size > 0;

    }

}