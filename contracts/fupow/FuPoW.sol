pragma solidity ^0.4.16;

import "contracts/fupow/Level.sol";
import "contracts/interfaces/IRareToken.sol";
import "contracts/interfaces/IFuPoW.sol";
import "contracts/interfaces/ICommonToken.sol";
import "contracts/interfaces/ILevel.sol";

/*

    Fun Proof of Work

    A way of mining and distributing tokens with object recognition

    Hashes here represent the hashes of social content submitted by other dApps and analyzed or hashes of names of objects scanned with the Find Bill dApp

*/

contract FuPoW is IFuPoW {

    //Which dApps allow users to do processing on the client side to see if they found the object and then reward them
    mapping (address => bool) clientSideProcessing;

    //Where dApps submit user data to be analyzed
    address opi;

    //The address of the token that is minted
    address rareTokenAddress;

    address commonTokenAddress;

    //The "chain" of levels
    address[] levels;

    //Set at 10 minutes
    uint256 optimalLevelDuration = 3;

    //How much did the last level take
    uint256 lastLevelDuration = 0;

    //How many objects need to be found in max optimalLevelDuration seconds
    uint256 objectsToFind = 1;

    uint256 maxCreateLevelReward = 20;

    //At which level did someone find the previous object
    uint256 positionLastFind;

    //When was the last timestamp when someone found an object
    uint256 timestampLastFind;

    //When will the current round finish
    uint256 currentRoundDeadline;

    //When did the current round start
    uint256 currentRoundStart;

    //When was the difficulty of a level last updated
    uint256 lastAdjustmentPosition;

    //How many object were found since the last adjustment
    uint256 foundSinceLastAdjustment;

    //If a puzzle solution was found in less than half of levelDuration, we increment this variable
    uint256 foundEarlier;

    //How many people are playing right now
    uint256 playersOnline;

    uint256 blockHeightWhenFound = 0;

    //After how many levels we adjust difficulty again
    uint256 DIFFICULTY_ADJUSTMENT_INTERVAL = 6;

    //Was the current answer found?
    bool foundCurrentAnswer = false;

    bool someoneCreatingLevel = false;

    modifier allowedPlatforms {

        require(clientSideProcessing[msg.sender] == true || msg.sender == opi);

        _;

    }

    modifier onlyOPI {

        require(msg.sender == opi);
        _;

    }

    function FuPoW(address rareTokenAddr, address commonTokenAddr, uint256 timestampStartGame) {

        require(rareTokenAddr != address(0));
        require(commonTokenAddr != address(0));
        require(isContract(rareTokenAddr) == true);
        require(isContract(commonTokenAddr) == true);
        require(timestampStartGame >= now);

        uint256 end = timestampStartGame + optimalLevelDuration * 1 minutes;

        address newLevel = new Level(address(0), 0, timestampStartGame, end);

        levels.push(newLevel);

        rareTokenAddress = rareTokenAddr;

        commonTokenAddress = commonTokenAddr;

        currentRoundDeadline = end;

        currentRoundStart = timestampStartGame;

    }

    function setCommonToken(address common) public onlyOwner {

        commonTokenAddress = common;

    }

    function toggleClientSideProcessing(address clientSide) public onlyOwner {

        require(isContract(clientSide) == true);

        clientSideProcessing[clientSide] = !clientSideProcessing[clientSide];

        ToggledClient(clientSide, clientSideProcessing[clientSide]);

    }

    function createNewLevel() public {

        require(foundCurrentAnswer == true || now >= currentRoundDeadline);
        require(someoneCreatingLevel == false);

        someoneCreatingLevel = true;

        address newLevel;

        if (levels.length - lastAdjustmentPosition == DIFFICULTY_ADJUSTMENT_INTERVAL) {

            if (foundEarlier >= DIFFICULTY_ADJUSTMENT_INTERVAL / 2 && 
                    foundSinceLastAdjustment >= DIFFICULTY_ADJUSTMENT_INTERVAL / 2) {

                if (foundEarlier <= 3) {

                    objectsToFind = objectsToFind + 1;

                } else {

                    objectsToFind = objectsToFind + foundEarlier / 2;

                }

            } else if (foundSinceLastAdjustment < DIFFICULTY_ADJUSTMENT_INTERVAL / 2 && objectsToFind > 1) {

                objectsToFind = objectsToFind - 1;

            }

            lastAdjustmentPosition = levels.length;

            foundSinceLastAdjustment = 0;

            foundEarlier = 0;

        }

        //CREATE LEVEL

        uint256 start = now;

        newLevel = new Level(levels[levels.length - 1], levels.length, 
                start, start + optimalLevelDuration * 1 minutes);

        levels.push(newLevel);

        //REWARD CALLER

        uint256 reward = computeReward(now);

        if (reward > 0) {

             ICommonToken commonToken = ICommonToken(commonTokenAddress);

             commonToken.rewardCreateLevel(msg.sender, reward);

        }

        //RESTART DATA

        blockHeightWhenFound = 0;

        currentRoundDeadline = start + optimalLevelDuration * 1 minutes;

        currentRoundStart = start;

        foundCurrentAnswer = false;

        //CAN CREATE NEW LEVELS

        someoneCreatingLevel = false;

        CreatedNewLevel(start);

    }

    function computeReward(uint256 latestStamp) private view returns (uint256) {

        uint256 reference = currentRoundDeadline;

        if (foundCurrentAnswer == true) reference = timestampLastFind;

        if (latestStamp - reference > 60) return 0;

        else
            return maxCreateLevelReward * (100 - ((latestStamp - reference) * 100 / 60)) / 100;

    }

    function foundSolution(address platform, address who, uint256 platformShare, uint256 timestamp) public allowedPlatforms {

        require(currentRoundDeadline >= now);
        require(isContract(who) == false);
        require(platformShare < 50);
        require(foundCurrentAnswer == false);
        require(timestamp <= now + 1 minutes && timestamp >= now - 1 minutes);

        lastLevelDuration = timestamp - currentRoundStart;

        foundCurrentAnswer = true;

        blockHeightWhenFound = block.number;

        timestampLastFind = timestamp;

        positionLastFind = levels.length - 1;

        foundSinceLastAdjustment = foundSinceLastAdjustment + 1;

        if (timestamp - currentRoundStart <= (currentRoundDeadline - currentRoundStart) / 2) {

            foundEarlier = foundEarlier + 1;

        }

        //Mine tokens

        IRareToken rare = IRareToken(rareTokenAddress);

        rare.mine(platform, who, platformShare, 100 - platformShare);

        //Set winner to level

        ILevel lvl = ILevel(levels[levels.length - 1]);

        lvl.setWinner(who);

        FoundSolution(platform, who);

    }

    function addOPI(address _opi) public onlyOwner {

        opi = _opi;

    }

    function getLevelNumber() public view returns (uint256) {

        return levels.length - 1;

    }

    function getLevelDuration() public view returns (uint256) {

        return optimalLevelDuration;

    }

    function getPositionLastFind() public view returns (uint256) {

        return positionLastFind;

    }

    function getFoundEarlier() public view returns (uint256) {

        return foundEarlier;

    }

    function getTimestampLastFind() public view returns (uint256) {

        return timestampLastFind;

    }

    function getObjectsToFind() public view returns (uint256) {

        return objectsToFind;

    }

    function getBlockHeightWhenFound() public view returns (uint256) {

        return blockHeightWhenFound;

    }

    function getCurrentRoundDeadline() public view returns (uint256) {

        return currentRoundDeadline;

    }

    function getCurrentLevelAddress() public view returns (address) {

        return levels[levels.length - 1];

    }

    function getCurrentRoundStart() public view returns (uint256) {

        return currentRoundStart;

    }

    function getLastAdjustmentPosition() public view returns (uint256) {

        return lastAdjustmentPosition;

    }

    function getDifficultyAdjustmentInterval() public view returns (uint256) {

        return DIFFICULTY_ADJUSTMENT_INTERVAL;

    }

    function getFoundCurrentAnswer() public view returns (bool) {

        return foundCurrentAnswer;

    }

    function getPlayersOnline() public view returns (uint256) {

        return playersOnline;

    }

    function getCommonTokenAddress() public view returns (address) {

        return commonTokenAddress;

    }

    function getRareTokenAddress() public view returns (address) {

        return rareTokenAddress;

    }

    function levelTerminated() public view returns (bool) {

        if (now >= currentRoundDeadline) {

            return true;

        }

        return false;

    }

    function isContract(address addr) private returns (bool) {

        uint size;
        assembly { size := extcodesize(addr) }
        return size > 0;

    }


}