pragma solidity ^0.4.16;

import "contracts/interfaces/ILevel.sol";

contract Level is ILevel {

    struct Data {

        address previousLevel;
        address winner;

        uint256 index;
        
        uint256 startTimestamp;
        uint256 endTimestamp;

    }

    address creator;

    Data levelData;

    modifier onlyCreator {

        require(creator == msg.sender);
        _;

    }

    function Level(address _previousLevel, uint256 _index, uint256 _startTimestamp,
                   uint256 _endTimestamp) {

        creator = msg.sender;

        levelData = Data(_previousLevel, address(0), _index, _startTimestamp, _endTimestamp);

    }

    function setWinner(address _winner) public onlyCreator {

        levelData.winner = _winner;

        WinnerSet(msg.sender, _winner);

    }

    function getPreviousLevel() public view returns (address) {

        return levelData.previousLevel;

    }

    function getWinner() public view returns (address) {

        return levelData.winner;

    }

    function getIndex() public view returns (uint256) {

        return levelData.index;

    }

    function getStartTimestamp() public view returns (uint256) {

        return levelData.startTimestamp;

    }

    function getEndTimestamp() public view returns (uint256) {

        return levelData.endTimestamp;

    }

}