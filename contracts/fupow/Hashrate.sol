pragma solidity ^0.4.16;

import "contracts/zeppelin/math/SafeMath.sol";
import "contracts/interfaces/IHashRate.sol";

contract Hashrate is IHashRate {

    using SafeMath for uint256;

    mapping(address => bool) connectedClients;

    //Where the videos/images hashes come from
    address api;

    uint256 normalHashes;

    uint256 opiHashes;

    modifier onlyAPI {

        require(msg.sender == api);
        _;

    }

    modifier onlyClients {

        require(connectedClients[msg.sender] == true);
        _;

    }

    function Hashrate(address _api) {

        api = _api;

    }

    function setOPI(address _api) public onlyOwner {

        api = _api;

    }

    function setClientHashProvenance(address _client) public onlyOwner {

        connectedClients[_client] = !connectedClients[_client];

    }

    function addOPIHash(uint256 quantity) public onlyAPI {

        opiHashes = opiHashes.add(quantity);

        OPIHashes(quantity);

    }

    function addClientSideHash(uint256 quantity) public onlyClients {

        normalHashes = normalHashes.add(quantity);

        ClientSideHash(msg.sender, quantity);

    }

    function getNormalHashes() public view returns (uint256) {

        return normalHashes;

    }

    function getOpiHashes() public view returns (uint256) {

        return opiHashes;

    }

    function getTotalHashes() public view returns (uint256) {

        return normalHashes + opiHashes;

    }

}