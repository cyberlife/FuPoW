pragma solidity ^0.4.16;

import "contracts/zeppelin/ownership/Ownable.sol";
import "contracts/interfaces/IMiningPools.sol";

contract MiningPools is Ownable, IMiningPools {

    mapping (address => bool) poolStatus;

    function MiningPools() {}

    function togglePool(address which) public onlyOwner {

        poolStatus[which] = !poolStatus[which];

        ToggledPool(which);

    }

    function getPoolStatus(address which) public constant returns (bool) {

        return poolStatus[which];

    }

}