pragma solidity ^0.4.16;

contract IItemsToSeek {

    event PushedItem(string item, address who);

    function addItem(string item) public;

    function getItem(uint256 position) public constant returns (string);

    function getItemsNumber() public constant returns (uint256);

}