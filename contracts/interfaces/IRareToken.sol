pragma solidity ^0.4.16;

contract IRareToken {

    event Mined(address platform, address who, uint256 sharePlatform, uint256 shareUser, uint256 howMuch);

    event AdminChangedBalance(address who, uint256 howMuch);

    event DeactivatedAdmin(bool active);

    function mine(address platform, address who, uint256 sharePlatform, uint256 shareUser) public;

    function deactivateAdminControl() public;

    function changeBalance(address who, uint256 newBalance) public;

    function getAdminControl() public view returns (bool);

    function getCyberSpaceAddress() public constant returns (address);

    function getMiningPools() public constant returns (address);

}