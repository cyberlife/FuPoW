pragma solidity ^0.4.16;

import "contracts/zeppelin/ownership/Ownable.sol";

contract IHashRate is Ownable {
    
    event RestartLong();
    event RestartShort();
    event OPIHashes(uint256 howMany);
    event ClientSideHash(address client, uint256 howMany);

    function restartLong() public;

    function restartShort() public;

    function setOPI(address _api) public;

    function addOPIHash(uint256 quantity) public;

    function addClientSideHash(uint256 quantity) public;

    function getNormalHashes() public view returns (uint256);

    function getOpiHashes() public view returns (uint256);

    function getTotalHashes() public view returns (uint256);

}