pragma solidity ^0.4.16;

import "contracts/zeppelin/ownership/Ownable.sol";

contract IFuPoW is Ownable {

    event FoundSolution(address platform, address who);
    event ClientSideHashed(bool success, address client);

    event CreatedNewLevel(uint256 timestamp);

    event ToggledClient(address client, bool state);

    function toggleClientSideProcessing(address clientSide) public;

    function createNewLevel() public;

    function foundSolution(address platform, address who, uint256 platformShare, uint256 timestamp) public;

    function addOPI(address _opi) public;

    function getFoundCurrentAnswer() public view returns (bool);

}