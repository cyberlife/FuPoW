pragma solidity ^0.4.16;

import "contracts/zeppelin/ownership/Ownable.sol";

contract ICommonToken is Ownable {

    event EmptiedSink(address which, uint256 howManyTokens);
    event Rewarded(address who, uint256 howMuch);
    event RaisedMintAllowance(address who, uint256 howMuch, string reason);
    event LoweredMintAllowance(address who, uint256 howMuch, string reason);
    event Mint(address who, uint256 howMuch);
    event MintAndTransfer(address who, address target, uint256 howMuch);

    function toggleClient(address client) public;

    function raiseMintingLimit(address who, uint256 amount, string reason) public;

    function slashMintingLimit(address who, uint256 amount, string reason) public;

    function rewardCreateLevel(address who, uint256 howMuch) public;

    function mintFromClient(address who, uint256 howMuch) public;

    function mintAndTransferFromClient(address who, address target, uint256 howMuch) public;

    function getMaxToBeMinted(address user) public view returns (uint256);

    function toggleMoneySink(address moneySink) public;

    function emptyMoneySink(address which) public;

    function isContract(address addr) returns (bool);

}