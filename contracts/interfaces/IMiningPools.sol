pragma solidity ^0.4.16;

contract IMiningPools {

    event ToggledPool(address which);

    function togglePool(address which) public;

    function getPoolStatus(address which) public constant returns (bool);

}