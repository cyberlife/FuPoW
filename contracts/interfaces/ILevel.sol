pragma solidity ^0.4.16;

contract ILevel {

    event WinnerSet(address whoSetWinner, address winner);

    function setWinner(address _winner) public;

    function getPreviousLevel() public view returns (address);

    function getWinner() public view returns (address);

    function getIndex() public view returns (uint256);

    function getStartTimestamp() public view returns (uint256);

    function getEndTimestamp() public view returns (uint256);

}