pragma solidity ^0.4.16;

contract IPlayerDatabase {

    event AssignKeyToEmail(string emailhash, address assignedPublicKey);
    event AssignKeyToNumber(string numberHash, address assignedPublicKey);
    event FullSignUp(string emailhash, string passhash, address assignedPublicKey, string phoneNumber);

    function setProtocol(address _fupow) public;

    function fullSignup(string emailhash, string passhash, address assignedPublicKey, string phoneNumber) public;

    function emailAssignKey(string emailhash, address assignedPublicKey) public;

    function numberAssignKey(string numberHash, address assignedPublicKey) public;

    function login(string emailhash) public constant returns (string);

    function checkIfNumberIsAssigned(string numberHash) public constant returns (bool);

    function getAddressFromEmail(string emailhash) public constant returns (address);

    function getAddressFromNumber(string numberHash) public constant returns (address);

}