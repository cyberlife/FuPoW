pragma solidity ^0.4.16;

import "contracts/zeppelin/ownership/Ownable.sol";

contract ITokenPolicy is Ownable {

    event ChangedSlashingCase(string which, uint256 amount);
    event ChangedRewardCase(string which, uint256 amount);

    function changeSlashingCase(string slashCase, uint256 tokenAmount) public;

    function changeRewardCase(string rewardCase, uint256 tokenAmount) public;

    function getSlashingCase(string which) public view returns (uint256);

    function getRewardCase(string which) public view returns (uint256);

}