pragma solidity ^0.4.16;

import "contracts/zeppelin/ERC721/ERC721Token.sol";

contract Stickers is ERC721Token {

    address sys_admin;

    mapping(string => uint256) internal nameToTokenId;

    mapping(uint256 => string) internal idToTokenName;

    mapping(uint256 => uint256) internal initialPrices;

    event AddedTokenName(string _name, uint256 _id);

    event ChangedAdmin(address newAdmin);

    modifier onlySysAdmin {

        require(msg.sender == sys_admin);
        _;

    }
 
    function Stickers(string _name, string _symbol) 
        ERC721Token(_name, _symbol) public {

        sys_admin = msg.sender;

    }

    function changeAdmin(address newAdmin_) public onlySysAdmin {

        sys_admin = newAdmin_;

        ChangedAdmin(newAdmin_);

    }

    function mint(address _who, uint256 _tokenId) public onlySysAdmin {

        super._mint(_who, _tokenId);

    }

    function setURI(uint256 id, string uri) public onlySysAdmin {

        super._setTokenURI(id, uri);

    }

    function setTokenName(string _name, uint256 _id) public onlySysAdmin {

        idToTokenName[_id] = _name;
        nameToTokenId[_name] = _id;

        AddedTokenName(_name, _id);

    }

    function setInitialPrice(uint256 id, uint256 price) public onlySysAdmin {

        initialPrices[id] = price;

    }

    function getInitialPrice(uint256 id) public view returns (uint256) {

        return initialPrices[id];

    }

    function getCollectibleName(uint256 id) public view returns (string) {

        return idToTokenName[id];

    }

    function getIdFromName(string _name) public constant returns (uint256) {

        return nameToTokenId[_name];

    }

    function getAllIds() public constant returns (uint256[]) {

        return allTokens;

    }


}