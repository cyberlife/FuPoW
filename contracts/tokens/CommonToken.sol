pragma solidity ^0.4.16;

import "contracts/zeppelin/math/SafeMath.sol";
import "contracts/zeppelin/ERC20/StandardToken.sol";
import "contracts/interfaces/ICommonToken.sol";

contract CommonToken is StandardToken, ICommonToken {

    using SafeMath for uint256;

    string public name = "Common Token";
    string public symbol = "COMMON";
    
    uint256 public decimals = 18;

    address fupow;

    mapping(address => bool) moneySinks;
    mapping(address => bool) connectedClients;
    mapping(address => uint256) maxToBeMintedPerUser;

    address[] sinks;

    modifier onlyPow {

        require(fupow != address(0));
        require(msg.sender == fupow);
        _;

    }

    modifier onlyClient {

        require(connectedClients[msg.sender] == true);
        _;

    }

    function CommonToken() {}

    function toggleClient(address client) public onlyOwner {

        connectedClients[client] = !connectedClients[client];

    }

    function raiseMintingLimit(address who, uint256 amount, string reason) public onlyClient {

        maxToBeMintedPerUser[who] = maxToBeMintedPerUser[who].add(amount);

        RaisedMintAllowance(who, amount, reason);

    }

    function slashMintingLimit(address who, uint256 amount, string reason) public onlyClient {

        maxToBeMintedPerUser[who] = maxToBeMintedPerUser[who].sub(amount);

        LoweredMintAllowance(who, amount, reason);

    }

    function mintAndTransferFromClient(address who, address target, uint256 howMuch) public onlyClient {

        require(maxToBeMintedPerUser[who] >= howMuch);

        maxToBeMintedPerUser[who] = maxToBeMintedPerUser[who].sub(howMuch);

        totalSupply_ = totalSupply_.add(howMuch);
        balances[target] = balances[target].add(howMuch);

        MintAndTransfer(who, target, howMuch);

    }

    function mintFromClient(address who, uint256 howMuch) public onlyClient {

        require(maxToBeMintedPerUser[who] >= howMuch);

        maxToBeMintedPerUser[who] = maxToBeMintedPerUser[who].sub(howMuch);

        totalSupply_ = totalSupply_.add(howMuch);
        balances[who] = balances[who].add(howMuch);

        Mint(who, howMuch);

    }

    function setPow(address _fupow) public onlyOwner {

        fupow = _fupow;

    }

    function rewardCreateLevel(address who, uint256 howMuch) public onlyPow {

        require(isContract(who) == false);

        totalSupply_ = totalSupply_.add(howMuch * 10**decimals);

        balances[who] = balances[who].add(howMuch * 10**decimals);

    }

    function toggleMoneySink(address moneySink) public onlyOwner {

        require(isContract(moneySink) == true);

        moneySinks[moneySink] = !moneySinks[moneySink];

        if (moneySinks[moneySink] == true) {

            sinks.push(moneySink);

        }

    }

    function emptyMoneySink(address which) public onlyOwner {

        require(moneySinks[which] == true);
        require(balanceOf(which) > 0);

        uint256 tokensToFlush = balances[which];

        totalSupply_ = totalSupply_.sub(balances[which]);

        balances[which] = 0;

        EmptiedSink(which, tokensToFlush);

    }

    function getMaxToBeMinted(address user) public view returns (uint256) {

        return maxToBeMintedPerUser[user];

    }

    function isContract(address addr) returns (bool) {

        uint size;
        assembly { size := extcodesize(addr) }
        return size > 0;

    }

}