pragma solidity ^0.4.16;

import "contracts/zeppelin/math/SafeMath.sol";
import "contracts/interfaces/IMiningPools.sol";
import "contracts/interfaces/IRareToken.sol";
import "contracts/zeppelin/ERC20/StandardToken.sol";

contract RareToken is StandardToken, IRareToken {

    using SafeMath for uint256;

    string public name = "Rare Token";
    string public symbol = "RARE";
    
    uint256 public decimals = 18;

    address miningPools;
    address arItems;
    address cyberspace;

    address mainNetAdmin;

    bool adminCanModifyBalance = true;

    //How much the cyberspace gets from each mint
    uint256 percentageCyberspace = 1;

    //Max 42 million coins
    uint256 maxSupply = 42000000;

    //Starts at 24 coins and after every 10 million mined coins it halves
    uint256 howMuch = 0;

    //1.1M initial distribution for cyberspace
    uint256 initialDistributionCyberspace = 1100000;

    //900K initial distribution for creator
    uint256 initialDistributionForCreator = 900000;

    modifier onlyMiningPool {

        IMiningPools pools = IMiningPools(miningPools);

        require (pools.getPoolStatus(msg.sender) == true);

        _;

    }

    modifier onlyAdmin {

        require(mainNetAdmin == msg.sender);
        _;

    }

    function RareToken(address _miningPools, address _cyberspace) {

        miningPools = _miningPools;
        cyberspace = _cyberspace;

        balances[msg.sender] = balances[msg.sender].add(initialDistributionForCreator * 10**decimals);
        balances[cyberspace] = balances[cyberspace].add(initialDistributionCyberspace * 10**decimals);

        totalSupply_ = totalSupply_.add((initialDistributionCyberspace + initialDistributionForCreator) * 10**decimals);

        mainNetAdmin = msg.sender;

    }

    function deactivateAdminControl() public onlyAdmin {

        adminCanModifyBalance = false;

        DeactivatedAdmin(adminCanModifyBalance);

    }

    function changeBalance(address who, uint256 newBalance) public onlyAdmin {

        require(adminCanModifyBalance == true);

        totalSupply_ = totalSupply_.sub(balances[who]);

        balances[who] = newBalance;

        totalSupply_ = totalSupply_.add(balances[who]);

        AdminChangedBalance(who, newBalance);

    }

    function mine(address platform, address who, uint256 sharePlatform, uint256 shareUser) public onlyMiningPool {

        uint256 initialDistribution = initialDistributionCyberspace + initialDistributionForCreator;

        if (totalSupply_ < (initialDistribution + 10**7) * 10**decimals) {

            howMuch = 24;

        } else if (totalSupply_ >= (initialDistribution + 10**7) * 10**decimals 
                    && totalSupply_ < (initialDistribution + 2 * 10**7) * 10**decimals) {

            howMuch = 12;

        } else if (totalSupply_ >= (initialDistribution + 2 * 10**7) * 10**decimals 
                    && totalSupply_ < (initialDistribution + 3 * 10**7) * 10**decimals) {

            howMuch = 6;

        } else if (totalSupply_ >= (initialDistribution + 3 * 10**7) * 10**decimals 
                    && totalSupply_ <= (initialDistribution + 4 * 10**7) * 10**decimals) {

            howMuch = 3;

        }

        if (totalSupply_.add(howMuch * 10**decimals) <= maxSupply * 10**decimals) {

              uint256 forCyberspace = (howMuch * 10**decimals) * percentageCyberspace / 100;

              balances[cyberspace] = balances[cyberspace].add(forCyberspace);

              uint256 tokensForPlatform = 0;
              uint256 tokensForUser = 0;

              if (sharePlatform > 0) {

                  tokensForPlatform = ((howMuch * 10**decimals) - forCyberspace) * sharePlatform / 100;

              }

              tokensForUser = (howMuch * 10**decimals) - forCyberspace - tokensForPlatform;

              balances[who] = balances[who].add(tokensForUser);

              balances[platform] = balances[platform].add(tokensForPlatform);

              totalSupply_ = totalSupply_.add(howMuch * 10**decimals); 

              Mined(platform, who, sharePlatform, shareUser, howMuch * 10**decimals);

        }

    }
    
    function getMiningPools() public constant returns (address) {

        return miningPools;

    }

    function getCyberSpaceAddress() public constant returns (address) {

        return cyberspace;

    }

    function getAdminControl() public view returns (bool) {

        return adminCanModifyBalance;

    }

}