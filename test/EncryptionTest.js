const EthCrypto = require('eth-crypto');

var labels = ["???","person","bicycle","car","motorcycle","airplane","bus","train","truck",
    "boat","traffic light","fire hydrant","???","stop sign","parking meter","bench","bird","cat","dog","horse","sheep",
    "cow","elephant","bear","zebra","giraffe","???","backpack","umbrella","???","???","handbag","tie","suitcase",
    "frisbee","skis","snowboard","sports ball","kite","baseball bat","baseball glove","skateboard","surfboard",
    "tennis racket","bottle","???","wine glass","cup","fork","knife","spoon","bowl","banana","apple","sandwich",
    "orange","broccoli","carrot","hot dog","pizza","donut","cake","chair","couch","potted plant","bed",
    "???","dining table","???","???","toilet","???","tv","laptop","mouse","remote","keyboard","cell phone",
    "microwave","oven","toaster","sink","refrigerator","???","book","clock","vase","scissors","teddy bear",
    "hair drier","toothbrush"];

contract('Encryption', function(accounts) {

    it("Encrypt and decrypt labels with multiple keys", async () => {

        var identities = [];
        var encryptedLabels = [];
        var decryptedLabels = [];

        console.log("List size: " + labels.length);

        //Create keys

        for (var i = 0; i < labels.length; i++) {

            identities.push(EthCrypto.createIdentity());

        }

        //Encrypt labels

        console.log("Encrypted labels:" + "\n");

        for (var i = 0; i < labels.length; i++) {

            encryptedLabels.push(await EthCrypto.encryptWithPublicKey(
                    
                    identities[i].publicKey, // publicKey
                    labels[i])
        
            );

            console.log(encryptedLabels[i].ciphertext);

        }

        console.log("\n" + "Labels with associated encrypted labels:" + "\n");

        //Labels with associated encrypted labels

        for (var i = 0; i < labels.length; i++) {

            console.log(labels[i] + " " + encryptedLabels[i].ciphertext);

        }

        console.log("\n" + "Generated keys: ");

        for (var i = 0; i < identities.length; i++) {

            console.log(identities);

        }

        //Decrypt data and show it

        console.log("\n" + "Decrypted labels: " + "\n");

        for (var i = 0; i < encryptedLabels.length; i++) {

            decryptedLabels.push(await EthCrypto.decryptWithPrivateKey(
                    identities[i].privateKey, // privateKey
                    encryptedLabels[i]
            )
        
        );

            console.log(labels[i] + ": " + decryptedLabels[i]);

        }

    });

});