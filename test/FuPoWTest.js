const FuPoW = artifacts.require("contracts/fupow/FuPoW.sol");
const MiningPools = artifacts.require("contracts/MiningPools.sol");
const RareToken = artifacts.require("contracts/tokens/RareToken.sol");
const CommonToken = artifacts.require("contracts/tokens/CommonToken.sol");
const ClientMining = artifacts.require("contracts/fupow/clientSideMine/ClientMining.sol");
const Hashrate = artifacts.require("contracts/fupow/Hashrate.sol");

const assertFail = require("./helpers/assertFail");

const increaseTime = require('./helpers/increaseTime');

var events = require("./helpers/listenToEvent");

contract('FuPoW', function(accounts) {

    let fupow;
    let miningPools;
    let rare;
    let commonToken;
    let client;
    let hashPool;

    let timeStamp, block;

    beforeEach(async () => {

        miningPools = await MiningPools.new({from: accounts[0]});

        block = web3.eth.blockNumber;

        timeStamp = web3.eth.getBlock(block).timestamp + 30;

        hashPool = await Hashrate.new(accounts[3], {from: accounts[0]});

        rare = await RareToken.new(miningPools.address, accounts[1], {from: accounts[0]});

        commonToken = await CommonToken.new({from: accounts[0]});

        fupow = await FuPoW.new(rare.address, commonToken.address, timeStamp, {from: accounts[0]});

        client = await ClientMining.new(accounts[0], fupow.address, 50, hashPool.address, {from: accounts[2]});

        await client.modifyRevenueShares(5, {from: accounts[2]});

        await commonToken.setPow(fupow.address, {from: accounts[0]});

        await hashPool.setClientHashProvenance(client.address, {from: accounts[0]});

        await miningPools.togglePool(fupow.address, {from: accounts[0]});

        await fupow.toggleClientSideProcessing(client.address, {from: accounts[0]});

    });

    it("Check RareToken consistency", async () => {

        console.log("Creator balance: " + await rare.balanceOf(accounts[0]));

        console.log("Cyberspace balance: " + await rare.balanceOf(accounts[1]));

    });

    it("Unrealistic test level difficulty adjustment -- automated", async () => {

        for (var i  = 1; i <= 500; i++) {

            console.log("#" + i);

            console.log("Account[0] common balance: " + await commonToken.balanceOf(accounts[0]));
        
            console.log("Start: " + await fupow.getCurrentRoundStart());
            
            console.log("End: " + await fupow.getCurrentRoundDeadline());
            
            console.log("Current: " + web3.eth.getBlock(web3.eth.blockNumber).timestamp);
            
            console.log("Level duration: " + await fupow.getLevelDuration());
            
            console.log("Last time difficulty update: " + await fupow.getLastAdjustmentPosition());
            
            console.log("Objects found earlier than half the time: " + await fupow.getFoundEarlier());

            var endTime = await fupow.getCurrentRoundDeadline();

            var remainingTime = endTime - web3.eth.getBlock(web3.eth.blockNumber).timestamp;
            
            var targetTime = Math.floor((Math.random() * (remainingTime - 5)) + 1);

            await increaseTime.increaseTimeTo(web3.eth.getBlock(web3.eth.blockNumber).timestamp + targetTime);
            
            console.log("After increase time: " + web3.eth.getBlock(web3.eth.blockNumber).timestamp);

            var foundSolution = Math.floor((Math.random() * 10) + 1);

            if (foundSolution > 5 && web3.eth.getBlock(web3.eth.blockNumber).timestamp < endTime - 1) {

                console.log("Found the item");

                await client.work("proof", true, accounts[3], 1, web3.eth.getBlock(web3.eth.blockNumber).timestamp, 
                    {from: accounts[2]});

            } else {

                console.log("Did not find item");

            }

            console.log("User rare balance: " + await rare.balanceOf(accounts[3]));

            console.log("Platform rare balance: " + await rare.balanceOf(accounts[2]));

            console.log("Cyberspace rare balance: " + await rare.balanceOf(await rare.getCyberSpaceAddress()));
            
            await increaseTime.increaseTimeTo(endTime);
            
            console.log("\n");
            
            await fupow.createNewLevel({from: accounts[0]});

         }

    });

});