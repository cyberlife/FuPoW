const Stickers = artifacts.require("contracts/tokens/Stickers.sol");

contract('Stickers', function(accounts) {

    let stickers;

    beforeEach(async () => {

        stickers = await Stickers.new("Stickers", "STK", {from: accounts[0]});

    });

    it("Setup new token and transfer it", async () => {

        await stickers.setTokenName("wine", 0, {from: accounts[0]});

        await stickers.mint(accounts[1], 0, {from: accounts[0]});

        await stickers.setURI(0, "someURI");

        assert.equal(await stickers.exists(0), true);

        await stickers.safeTransferFrom(accounts[1], accounts[2], 0, {from: accounts[1]});

        assert.equal(await stickers.ownerOf(0), accounts[2]);

    });

});