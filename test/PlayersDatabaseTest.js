const PLayerDatabase = artifacts.require("contracts/fupow/clientSideMine/PlayerDatabase.sol");

contract('PlayerDatabase', function(accounts) {

    let database;

    beforeEach(async () => {

        database = await PLayerDatabase.new({from: accounts[0]});

    });

    it("Register and login", async () => {

        //User #1

        await database.fullSignup("email", "pass", "0x123456789", "", {from: accounts[0]});

        let password = await database.login("email");
        
        assert.equal(password, "pass");

        let address = await database.getAddressFromEmail("email");

        assert.equal(address, "0x0000000000000000000000000000000123456789");


        //User #2

        await database.fullSignup("salam@yahoo.com", "12345678", "0x123", "", {from: accounts[0]});

        password = await database.login("salam@yahoo.com");
        
        assert.equal(password, "12345678");

        address = await database.getAddressFromEmail("salam@yahoo.com");

        assert.equal(address, "0x0000000000000000000000000000000000000123");

    });

});