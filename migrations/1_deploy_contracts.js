//TOKENS

var RareToken = artifacts.require('contracts/tokens/RareToken.sol');
var Stickers = artifacts.require('contracts/tokens/Stickers.sol');
var CommonToken = artifacts.require('contracts/tokens/CommonToken.sol');

//POW ALGO

var FuPoW = artifacts.require('contracts/fupow/FuPoW.sol');
var MiningPools = artifacts.require('contracts/MiningPools.sol');
var Hashrate = artifacts.require('contracts/fupow/Hashrate.sol');

//OPI

var FuOPI = artifacts.require('contracts/fupow/opiMine/FuOPI.sol');
var FuOPIPrices = artifacts.require('contracts/fupow/opiMine/FuOPIPrices.sol');

//AI

var CyberTreasury = artifacts.require('contracts/cyberspace/CyberTreasury.sol');

//CLIENT

var ClientManager = artifacts.require('contracts/client/ClientManager.sol');
var TokenPolicy = artifacts.require('contracts/client/TokenPolicy.sol');
var ClientMining = artifacts.require('contracts/clientSideMine/ClientMining.sol');
var PlayerDatabase = artifacts.require('contracts/fupow/clientSideMine/PlayerDatabase.sol');

module.exports = async function(deployer, network, accounts) {

    var sys_admin = "0x1900a41f2777ab70aad2074e3F4B9c5429c7f243";
    var zero = "0x0";

    var hashedNumber = web3.sha3("+447775456404");

    var secondHash = web3.sha3("07775456404");

    var emailHash = web3.sha3("codrinionescu@yahoo.com");

    if (network == "main") {

        await deployer.deploy(CyberTreasury);

        await deployer.deploy(MiningPools);

        await deployer.deploy(CommonToken);

        await deployer.deploy(RareToken, MiningPools.address, CyberTreasury.address);

        await deployer.deploy(Stickers, "Stickers", "STK");

        //CyberTreasury

        await CyberTreasury.at(CyberTreasury.address)
            .setMineableToken(RareToken.address);

    } else if (network == "rinkeby" || network == "ropsten") {

        await deployer.deploy(CommonToken);

        await deployer.deploy(MiningPools);

        await deployer.deploy(CyberTreasury);

        await deployer.deploy(RareToken, MiningPools.address, CyberTreasury.address);

        await deployer.deploy(FuPoW, RareToken.address, CommonToken.address, 1534778502);

        await deployer.deploy(Stickers, "Stickers", "STK");

        await deployer.deploy(ClientManager);

        await deployer.deploy(TokenPolicy);

        await deployer.deploy(PlayerDatabase);

        await deployer.deploy(Hashrate, sys_admin);

        await deployer.deploy(ClientMining, PlayerDatabase.address, FuPoW.address, 50, Hashrate.address);

        await deployer.deploy(FuOPIPrices);

        await deployer.deploy(FuOPI, FuPoW.address, RareToken.address, 
            FuOPIPrices.address, Hashrate.address);

        //Client Manager

        await ClientManager.at(ClientManager.address)
            .setCommonToken(CommonToken.address);

        await ClientManager.at(ClientManager.address)
            .addSubContract(ClientMining.address, "clientMining");

        await ClientManager.at(ClientManager.address)
            .addSubContract(PlayerDatabase.address, "userDatabase");

        await ClientManager.at(ClientManager.address)
            .addSubContract(TokenPolicy.address, "tokenPolicy");
            
        //Client Mining

        await ClientMining.at(ClientMining.address)
            .modifyRevenueShares(10);

        //TokenPolicy

        await TokenPolicy.at(TokenPolicy.address)
            .changeRewardCase("signUp", 100);

        //CommonToken

        await CommonToken.at(CommonToken.address)
            .setPow(FuPoW.address);

        await CommonToken.at(CommonToken.address)
            .toggleClient(ClientManager.address);

        //CyberTreasury

        await CyberTreasury.at(CyberTreasury.address)
            .setMineableToken(RareToken.address);

        //Rare Token

        await RareToken.at(RareToken.address)
            .deactivateAdminControl();

        //Mining pool

        await MiningPools.at(MiningPools.address)
            .togglePool(FuPoW.address);

        //Hashrate

        await Hashrate.at(Hashrate.address)
            .setAPI(FuOPI.address);

        await Hashrate.at(Hashrate.address)
            .setClientHashProvenance(ClientMining.address);

        //Player database

        await PlayerDatabase.at(PlayerDatabase.address)
            .setProtocol(FuPoW.address);

        //FuOPI

        await FuOPI.at(FuOPI.address)
            .setTokenSink(zero);

        //FuPoW

        await FuPoW.at(FuPoW.address)
            .toggleClientSideProcessing(ClientMining.address);

        await FuPoW.at(FuPoW.address)
            .addOPI(FuOPI.address);

        //Stickers

        await Stickers.at(Stickers.address)
          .setTokenName("wine", 0);

        await Stickers.at(Stickers.address)
            .setTokenName("bottle", 1);

        await Stickers.at(Stickers.address)
            .setTokenName("microwave", 2);

        await Stickers.at(Stickers.address)
            .setTokenName("banana", 3);

        await Stickers.at(Stickers.address)
            .setTokenName("oven", 4);

        await Stickers.at(Stickers.address)
            .setTokenName("fridge", 5);

        await Stickers.at(Stickers.address)
            .setTokenName("laptop", 6);

        await Stickers.at(Stickers.address)
            .setTokenName("teddy_bear", 7);

        for (var i = 0; i <= 7; i++) {

            await Stickers.at(Stickers.address)
                .setInitialPrice(i, 5);
    
        }

    } else if (network == "development") {

        await deployer.deploy(CyberTreasury, {from: accounts[0]});

        await deployer.deploy(ClientManager, {from: accounts[0]});

        await deployer.deploy(TokenPolicy, {from: accounts[0]});

        await deployer.deploy(MiningPools, {from: accounts[0]});

        await deployer.deploy(RareToken, MiningPools.address, CyberTreasury.address, {from: accounts[0]});

        await deployer.deploy(PlayerDatabase, {from: accounts[0]});

        await deployer.deploy(CommonToken, {from: accounts[0]});

        await deployer.deploy(FuPoW, RareToken.address, CommonToken.address, 9534177740, {from: accounts[0]});

        await deployer.deploy(Stickers, "ARItems", "ARI", {from: accounts[0]});

        await deployer.deploy(Hashrate, accounts[2], {from: accounts[0]});

        await deployer.deploy(ClientMining, PlayerDatabase.address, FuPoW.address, 50, Hashrate.address, {from: accounts[0]});

        await deployer.deploy(FuOPIPrices, {from: accounts[0]});

        await deployer.deploy(FuOPI, FuPoW.address, RareToken.address, 
            FuOPIPrices.address, Hashrate.address, {from: accounts[0]});

        //Client Mining

        await ClientMining.at(ClientMining.address)
            .modifyRevenueShares(10);

        //Client Manager

        await ClientManager.at(ClientManager.address)
            .setCommonToken(CommonToken.address);

        await ClientManager.at(ClientManager.address)
            .addSubContract(ClientMining.address, "clientMining");

        await ClientManager.at(ClientManager.address)
            .addSubContract(PlayerDatabase.address, "userDatabase");

        await ClientManager.at(ClientManager.address)
            .addSubContract(TokenPolicy.address, "tokenPolicy");
            
        //TokenPolicy

        await TokenPolicy.at(TokenPolicy.address)
            .changeRewardCase("signUp", 100);

        //CommonToken

        await CommonToken.at(CommonToken.address)
            .setPow(FuPoW.address);

        await CommonToken.at(CommonToken.address)
            .toggleClient(ClientManager.address);

        //CyberTreasury

        await CyberTreasury.at(CyberTreasury.address)
            .setMineableToken(RareToken.address);

        //Mining pool

        await MiningPools.at(MiningPools.address)
            .togglePool(FuPoW.address);

        //Hashrate

        await Hashrate.at(Hashrate.address)
            .setAPI(FuOPI.address);

        await Hashrate.at(Hashrate.address)
            .setClientHashProvenance(ClientMining.address);

        //Player database

        await PlayerDatabase.at(PlayerDatabase.address)
            .setProtocol(FuPoW.address);

        await PlayerDatabase.at(PlayerDatabase.address)
            .numberAssignKey(hashedNumber, sys_admin);

        await PlayerDatabase.at(PlayerDatabase.address)
            .numberAssignKey(secondHash, sys_admin);

        await PlayerDatabase.at(PlayerDatabase.address)
            .emailAssignKey(emailHash, sys_admin);

        //FuOPI

        await FuOPI.at(FuOPI.address)
            .setTokenSink(zero);

        //FuPoW

        await FuPoW.at(FuPoW.address)
            .toggleClientSideProcessing(ClientMining.address);

        await FuPoW.at(FuPoW.address)
            .addOPI(FuOPI.address);

        //Stickers

        await Stickers.at(Stickers.address)
            .setTokenName("wine", 0);

        await Stickers.at(Stickers.address)
            .setTokenName("bottle", 1);

        await Stickers.at(Stickers.address)
            .setTokenName("microwave", 2);

        await Stickers.at(Stickers.address)
            .setTokenName("banana", 3);

        await Stickers.at(Stickers.address)
            .setTokenName("oven", 4);

        await Stickers.at(Stickers.address)
            .setTokenName("fridge", 5);

        await Stickers.at(Stickers.address)
            .setTokenName("laptop", 6);

        await Stickers.at(Stickers.address)
            .setTokenName("teddy_bear", 7);

        for (var i = 0; i <= 7; i++) {

            await Stickers.at(Stickers.address)
                .setInitialPrice(i, 5);

        }

    }

}
